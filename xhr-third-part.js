document.getElementById('btnUser').addEventListener('click', getUser);
document.getElementById('btnUsers').addEventListener('click', getUsers);

function getUser() {
    const xhr = new XMLHttpRequest();
    xhr.open('GET', 'https://randomuser.me/api', true);
    xhr.onload = function () {
        if (this.status === 200) {
            const user = JSON.parse(this.responseText);
            let output = `
                <ul>
                    <li>Name: ${user.results[0].name.title} ${user.results[0].name.first} ${user.results[0].name.last} </li>
                    <li>Gender: ${user.results[0].gender}</li>
                    <li>Country:${user.results[0].location.country} </li>
                    <li>Country:${user.results[0].email} </li>
                    <li>Country:${user.results[0].dob.age} </li>
                    <li><img src="${user.results[0].picture.thumbnail}"> </li>
                </ul>
        `;
            document.getElementById('user').innerHTML = output;
        }
    };
    xhr.send();

}

function getUsers(){
    const xhr=new XMLHttpRequest();
    let count=document.getElementById('count').value;
    if(count=="" || count<=0){
        count=5;
        count.value=5;
    }
    xhr.open('GET',`https://randomuser.me/api/?results=${count}`,true);
    xhr.onload=function(){
        if(this.status === 200){
            const users=JSON.parse(this.responseText);
            let output="";
            users.results.forEach(function(user){
            output +=`       
                        <ul>
                        <li>Name: ${user.name.title} ${user.name.first} ${user.name.last} </li>
                        <li>Gender: ${user.gender}</li>
                        <li>Country: ${user.location.country}</li>
                        <li>Email: ${user.email}</li>
                        <li>Age: ${user.dob.age}</li>
                        <li><img src="${user.picture.thumbnail}" alt="user Image"></li>
                    </ul>

`;
});
            document.getElementById('users').innerHTML=output;
        }
    };
    xhr.send();
}