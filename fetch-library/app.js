const http = new slHTTP();

// http.get('https://jsonplaceholder.typicode.com/posts')
//     .then(posts => console.log(posts))
//     .catch(err => console.log(err));

//Create some dummy data
const data = {
    userId: 17,
    title: 'Welcome to Asynch JS',
    body: 'et iusto sed quo iure voluptatem occaecati omnis eligendi aut ad voluptatem doloribus vel accusantium quis pariatur molestiae porro eius odio et labore et velit aut'
}

// http.post('https://jsonplaceholder.typicode.com/posts', data)
//     .then(post => console.log(post))
//     .catch(err => console.log(err));

// http.put('https://jsonplaceholder.typicode.com/posts/4', data)
//     .then(post => console.log(post))
//     .catch(err => console.log(err));

http.delete('https://jsonplaceholder.typicode.com/posts/4')
    .then(post => console.log(post))
    .catch(err => console.log(err));
