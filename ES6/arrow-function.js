//const sayHello = function(){
//	console.log("hello");
//}

//const sayHello = () => {
//	console.log("hello");
//}


const sayHello = () => console.log("hello");

//const returnHello = function(){
//	return "returned hello";
//}

const returnHello = () =>  "returned hello";


//RETURNED AN OBJECT

//const returnObject = function(){
//	return {name: "john discik", age: 19};
//}return is wrtieen as ()

const returnObject = () =>  ({name: "john dick", age: 19});

//FUNCTION WITH ARGUMENTS

//const sayWelcome = function(fullName){
//	console.log("welcome: ",fullName);
//}

const sayWelcome = (fullName) => console.log("welcome: ",fullName)

//const printWelcome = function(firstName , lastName){
//	console.log(`Welcome ${firstName} ${lastName}`);
//}

const printWelcome = (firstName , lastName) => console.log(`Welcome ${firstName} ${lastName}`);

const delayedMsg = setTimeout(() => console.log("hello world after 2 seconds"),2000);

const names = ['Harry','Potter','chandan','idk'];

//const nameslength = names.map(function(name){
//	return name.length;
//})

const namesLength = names.map(name =>  name.length);

console.log(namesLength);

//sayHello();

//console.log(returnHello());

//console.log(returnObject());

//sayWelcome("chandan panjwani");

//printWelcome("chan","panjwani")