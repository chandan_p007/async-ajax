let students = [
	{ name: 'John Doe',age: 27 },
	 { name: 'Jack Doe',age: 97 }
];

function createStudent(student){
	return new Promise(function(resolve,reject){
		setTimeout(function(){
			students.push(student);
			const error = false;
			if(error){
				reject('ERROR STATUS')
			}else{
				resolve();
			}
		},2100)
	});
}

function getStudent(){
//	display all the students
	let output="";
	students.forEach(function(student){
		output += `
	${student.name}`;
		
	});
	document.body.innerHTML = output;
	
}

createStudent({name: 'Jimmy Doe',age: 01})
.then(function(){
	getStudent();
})	
.catch(function(){
	console.error("ERROR OCCURED");
});