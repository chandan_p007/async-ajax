document.getElementById('btnStudent').addEventListener('click',getStudent);
document.getElementById('btnStudents').addEventListener('click',getStudents);

function getStudent(){
	const xhr = new XMLHttpRequest();
	
	xhr.open('GET' , 'student.json',true);
	
	xhr.onload = function(){
		if(this.status === 200){
//			console.log(this.responseText);
			const student = JSON.parse(this.responseText);
//			console.log(student);
			
			let output = `
					<ul>
							<li>ID: ${student.id}</li>
							<li>Name: ${student.name}</li>
							<li>Company: ${student.company}</li>
							<li>Phone: ${student.phone}</li>
					</ul>
						`;
			document.getElementById('student').innerHTML=output;
		}
	}
	xhr.send();
}

function getStudents(){
	const xhr = new XMLHttpRequest();
	
	xhr.open('GET' , 'students.json',true);
	
	xhr.onload = function(){
		if(this.status === 200){
//			console.log(this.responseText);
			const students = JSON.parse(this.responseText);
//			console.log(student);
			
			let output = "";
			students.forEach(function(student){
				output +=  `
					<ul>
							<li>ID: ${student.id}</li>
							<li>Name: ${student.name}</li>
							<li>Company: ${student.company}</li>
							<li>Phone: ${student.phone}</li>
					</ul>
						`;
			})
			document.getElementById('students').innerHTML=output;
		}
	}
	xhr.send();
}