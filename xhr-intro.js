document.getElementById('button').addEventListener('click',loadData);

function loadData(){
//	console.log("clicked");
	
	
//	creating a XHR Object
	const xhr = new XMLHttpRequest();
	
//	OPEN THE CONNECTION TO REMOTE USING XHR
//	SYNTAX
//	open(method,url[,async[,username[,password]]])
	
	console.log("READYSTATE : ",xhr.readyState);


//Ready State

//0: request not initialized
//1: server connection established
//2:request recieved
//3:Server processing request
//4:request finished and response is ready

xhr.open('GET','data.txt',true);
console.log("READYSTATE : ",xhr.readyState);

xhr.onload = function(){
	console.log("READYSTATE in onload: ",xhr.readyState);
//	console.log("hello world!!!!");
	
//	HTTP STATUSES
	
//	200 : OK
//	403: forbidden acces
	
//   === : for type casting	
	
	if(this.status === 200){
		console.log(this.responseText);
		document.getElementById('output').innerHTML=this.responseText;
	}
}

//it can be used to show spiinners and loaders

xhr.onprogress=function(){
	console.log("READYSTATE in progress: ",xhr.readyState);
}

//xhr.onreadystatechange=function(){
//	console.log("READYSTATE  changed: ",xhr.readyState);
//	if(this.status === 200 & this.onreadystate === 4){
//		document.getElementById('output').innerHTML=this.responseText;
//	}
//}

//send the request

xhr.send();
	
		}