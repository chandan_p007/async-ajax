function slHTTP(){
	this.http = new XMLHttpRequest();
	this.state=0;
}


slHTTP.prototype.get = function (url,callback) {
	this.http.open('GET',url,true);
	
	let self = this;
	this.http.onload = function(){
		if(self.http.status === 200){
			callback(null,self.http.responseText);
		}else{
			callback('ERROR : ' + self.http.status);
		}
	}
	this.http.send();
};

slHTTP.prototype.post = function (url,data,callback) {
	this.http.open('POST',url,true);
	this.http.setRequestHeader('Content-type' , 'application/json');
	let self = this;
	this.http.onload = function(){
		if(self.http.status === 200 || self.http.status === 201){
			callback(null, self.http.responseText);
		}else{
			callback('ERROR : ' + self.http.status);
		}
	}
	this.http.send(JSON.stringify(data));
};

slHTTP.prototype.put = function (url,data,callback) {
	this.http.open('PUT',url,true);
	this.http.setRequestHeader('Content-type' , 'application/json');
	let self = this;
	this.http.onload = function(){
		if(self.http.status === 200 || self.http.status === 201){
			callback(null,self.http.responseText);
		}else{
			callback('ERROR : ' + self.http.status);
		}
	}
	this.http.send(JSON.stringify(data));		
};

slHTTP.prototype.delete = function (url,callback) {
	this.http.open('DELETE',url,true);
	
	let self = this;
	this.http.onload = function(){
		if(self.http.status === 200){
			callback(null, 'Resource Deleted');
		}else{
			callback('ERROR : ' + self.http.status);
		}
	}
	this.http.send();
};
